import {Component, Input, OnInit} from '@angular/core';
import {element} from 'protractor';
import {log} from 'util';
import {reduce} from 'rxjs/operators';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss']
})
export class FavoriteComponent implements OnInit{

  favorite:any[];

  ngOnInit(): void {
    this.favorite=JSON.parse(localStorage.getItem('favoriteFilms'));
  }

  removeFromFavorite(film,index){
    this.favorite.splice(index , 1);
    localStorage.setItem('favoriteFilms', JSON.stringify(this.favorite));
  }

  total = () => this.favorite.reduce((total, film) => total + film.price, 0);

  clearLoc() {
    localStorage.clear();
    this.favorite = [];
  }
}


