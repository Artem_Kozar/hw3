import {Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {element} from 'protractor';
import construct = Reflect.construct;

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {

  movies: any[];
  favorite: any[] ;

  private _jsonURL = 'assets/movies.json';
  filmInFavorite: any;
  constructor(private http: HttpClient) {

  }
  public getJSON(): Observable<any> {
    return this.http.get(this._jsonURL);
  }
  ngOnInit(): void {
    this.favorite= localStorage.getItem('favoriteFilms') ? JSON.parse(localStorage.getItem('favoriteFilms')) : [];
    this.getJSON().subscribe(data => {
      this.movies = data;
      this.movies.forEach(m =>{
        this.filmCheck(m);
      })
    });
  }
  addToFavorite(film){
    this.favorite.push(film);
    this.filmCheck(film);
    localStorage.setItem('favoriteFilms', JSON.stringify(this.favorite));
  }

  removeFromFavorite(film){
    const index = this.favorite.findIndex(f => f.Id === film.Id);
    this.favorite.splice(index, 1);
    this.filmCheck(film);
    localStorage.setItem('favoriteFilms', JSON.stringify(this.favorite));
  }

  filmCheck(film){
    film.filmInFavorite = !!this.favorite.find(f => f.Id === film.Id);
  }

}






